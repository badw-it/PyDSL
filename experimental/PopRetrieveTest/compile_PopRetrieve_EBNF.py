#!/usr/bin/python3

"""compile_PopRetrieve.py - test of Pop and Retrieve operators 
                             

Author: Eckhart Arnold <arnold@badw.de>

Copyright 2017 Bavarian Academy of Sciences and Humanities

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys
sys.path.append(os.path.abspath('../../'))
from DHParser.dsl import compile_on_disk, is_outdated

if (not os.path.exists('PopRetrieve_compiler.py') or
    is_outdated('PopRetrieve_compiler.py', 'PopRetrieve.ebnf')):
    print("recompiling PopRetrieve parser")
    errors = compile_on_disk("PopRetrieve.ebnf")
    if errors:
        print('\n\n'.join(errors))
        sys.exit(1)


# from PopRetrieve_compiler import compile_PopRetrieve
#
# print("PopRetrieveTest 1")
# result, errors, ast = compile_PopRetrieve("PopRetrieveTest.txt")
# if errors:
#     print(errors)
#     sys.exit(1)
# else:
#     print(result)
#
# print("PopRetrieveTest 2")
# result, errors, ast = compile_PopRetrieve("PopRetrieveTest2.txt")
# if errors:
#     print(errors)
#     sys.exit(1)
# else:
#     print(result)


print("PopRetrieveTest 1")
errors = compile_on_disk("PopRetrieveTest.txt", 'PopRetrieve_compiler.py')
if errors:
    print(errors)
    sys.exit(1)

print("PopRetrieveTest 2")
errors = compile_on_disk("PopRetrieveTest2.txt", 'PopRetrieve_compiler.py')
if errors:
    print(errors)
    sys.exit(1)



if (not os.path.exists('PopRetrieveComplement_compiler.py') or
        is_outdated('PopRetrieveComplement_compiler.py', 'PopRetrieveComplement.ebnf')):
    print("recompiling PopRetrieveComplement parser")
    errors = compile_on_disk("PopRetrieveComplement.ebnf")
    if errors:
        print('\n\n'.join(errors))
        sys.exit(1)


from PopRetrieveComplement_compiler import compile_PopRetrieveComplement

print("PopRetrieveComplement Test 1")
result, errors, ast = compile_PopRetrieveComplement("PopRetrieveComplementTest.txt")
if errors:
    print(errors)
    sys.exit(1)
else:
    print(result)

print("PopRetrieveComplement Test 2")
result, errors, ast = compile_PopRetrieveComplement("PopRetrieveComplementTest2.txt")
if errors:
    print(errors)
    sys.exit(1)
else:
    print(result)
