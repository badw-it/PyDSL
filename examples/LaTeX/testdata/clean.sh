#!/usr/bin/sh

rm test*.dvi
rm test*.pdf
rm test*.run.*
rm test*.aux
rm test*.bbl
rm test*.bcf
rm test*.blg
rm test*.dvi
rm test*.log
rm test*.toc
