Mittellateinisches Wörterbuch
=============================

This directory contains the components for two kinds of DSLs for the 
[Medival Latin Dictionary](https://www.mlw.badw.de) of the 
[Bavarian Academy of Sciences](https://badw.de). The two kinds of DSLs are.

1. MLW: A domain specific language for writing dictionary entries
   for the "Mittellateinisches Wörterbuch". The DSL texts will be converted
   to an XML data model.

2. MLW_RETRO: A grammar for the retrodigitalisation of existing dictionary
   entries in ASCII-Format. This is very experimental...

